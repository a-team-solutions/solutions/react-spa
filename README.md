# A-Team Solutions React SPA

Rapid web application development React skeleton projekt.

## Advantages

- **Ultra fast** web app **project setup**
- **Rapid development** project structure, even for **junior deveopers**
- **Modularized application** structure
- **Scalable application** design
- **Responsive UI**
- **Testable code structure** - unit testing, component testing
- **Efective component development** using storybook
- **UI stories and components book** live preview

## Demo

- Applivation demo [https://a-team-solutions.gitlab.io/solutions/react-spa](https://a-team-solutions.gitlab.io/solutions/react-spa)

- Component preview [https://a-team-solutions.gitlab.io/solutions/react-spa/storybook](https://a-team-solutions.gitlab.io/solutions/react-spa/storybook)
