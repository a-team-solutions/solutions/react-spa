import i18next from 'i18next';
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

import moment from 'moment';
import numeral from 'numeral';

import 'moment/locale/sk';
import 'numeral/locales/sk';

function i18nInit(lang: string) {
    // moment.locale(lang);
    // numeral.locale(lang);

    // see: https://www.i18next.com/overview/configuration-options
    i18next
        .use(Backend)
        .use(LanguageDetector)
        .use(initReactI18next)
        .init({
            lng: lang,
            fallbackLng: lang,
            debug: false,
            interpolation: {
                format: function(value, format, lng) {
                    if (format === 'uppercase') {
                        return value.toUpperCase();
                    }
                    if (value instanceof Date) {
                        return moment(value as Date).format(format);
                    }
                    if (typeof value === 'number') {
                        return numeral(value as number).format(format);
                    }
                    return value;
                },
                escapeValue: false // not needed for react!!
            },
            backend: {
                loadPath: process.env.PUBLIC_URL + '/locales/{{lng}}.json'
            },
            load: 'languageOnly',
            // react: {
            //     wait: true,
            //     bindI18n: 'languageChanged loaded',
            //     useSuspense: false
            // },
            initImmediate: false
        });

    i18next.on('languageChanged', (lang: string) => {
        // console.log('languageChanged,', lang);
        moment.locale(lang);
        numeral.locale(lang);
    });

    // i18next.on("initialized", (options: any) => {
    //     console.info("i18n initialized", options);
    // });

    // i18next.on("failedLoading", (lang: Lang, ns: any, msg: string) => {
    //     console.error("Failed to load i18n resources", lang, ns, msg);
    // });

    // i18next.on("onMissingKey", (lang: Lang, ns: any, key: string, res: any) => {
    //     console.error("Failed to load i18n resources", lang, ns, key, res);
    // });

    return i18next;
}

export default i18nInit;

// export function format(message: string, ...data: string[]): string {
//     return data.reduce(
//         (msg, d, i) => msg.replace(`{${i}}`, d),
//         message
//     );
// }
