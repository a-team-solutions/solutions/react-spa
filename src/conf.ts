import { version } from '../package.json';
import { InitializeOptions } from 'react-ga';

export const confDefault = {
    version,
    settings: {
        lang: 'sk',
        name: ''
    },
    lang: 'en',
    langs: [
        'en',
        'sk'
    ],
    api: {
        basePath: 'api-mock'
    },
    googleAnalytics: {
        trackingCode: '',
        options: {} as InitializeOptions
    }
};

export type Conf = typeof confDefault;
