import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

storiesOf('Demo.Knobs', module)
    // Add the `withKnobs` decorator to add knobs support to your stories.
    // You can also configure `withKnobs` as a global decorator.
    .addDecorator(withKnobs)
    // Knobs for React props
    .add('button', () => (
        <button
            disabled={boolean('Disabled', false)}
            onClick={action('clicked')}
        >
            {text('Label', 'Hello Storybook')}
        </button>
    ))
    // Knobs as dynamic variables.
    .add('variables', () => {
        const name = text('Name', 'Arunoda Susiripala');
        const age = number('Age', 89);
        const content = `I am ${name} and I'm ${age} years old.`;
        return <div>{content}</div>;
    });
