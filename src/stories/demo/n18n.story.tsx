import { storiesOf } from '@storybook/react';
import React from 'react';
import i18next from 'i18next';
import { withTranslation, WithTranslation } from 'react-i18next';
import moment from 'moment';
import numeral from 'numeral';
import { withKnobs } from '@storybook/addon-knobs';

interface Props extends WithTranslation {}

function Message({ t }: Props) {
    return (
        <>
            {/* <div>{t('uppercase', { text: 'uppercase' })}</div> */}
            {/* <div>{t('date', { date: new Date() })}</div> */}
            {/* <div>{t('number', { number: 123456.789 })}</div> */}

            <div>
                {t('uppercase: {{text, uppercase}}', { text: 'uppercase' })}
            </div>
            <div>{t('date: {{date, LLL}}', { date: new Date() })}</div>
            <div>
                {t('number: {{number, 0,0.0[0000]}}', { number: 123456.789 })}
            </div>
            <hr />
            <div>{moment().format('LL')}</div>
            <div>{moment().format('LLL')}</div>
            <div>{moment().format('LLLL')}</div>
            <div>{moment().format('LTS')}</div>
            <div>{numeral(123456.789).format('0,0.0[0000]')}</div>
        </>
    );
}

const TMessage = withTranslation()(Message);

function changeLanguage(lang: string): void {
    i18next.changeLanguage(lang);
    // console.log(i18next.languages);
    // console.log(i18next.language);
    // console.log(moment(new Date()).format('MMMM Do YY'));
    // console.log(numeral(123456.789).format("0,0.0[0000]"));
    // console.log(i18next.t('uppercase', { text: 'uppercase' }));
    // console.log(i18next.t('date', { date: new Date() }));
    // console.log(i18next.t('number', { number: 123456.789 }));
    // console.log(i18next.t('uppercase: {{text, uppercase}}', { text: 'uppercase' }));
    // console.log(i18next.t('date: {{date, MMMM Do YY}}', { date: new Date() }));
    // console.log(i18next.t('number: {{number, 0,0.0[0000]}}', { number: 123456.789 }));
}

// i18nInit('sk');

storiesOf('Demo', module)
    .addDecorator(withKnobs)
    .add('i18n', () => (
        <div className="t-panel">
            <h1>I18N</h1>
            <button type="button" onClick={() => changeLanguage('sk')}>
                sk
            </button>{' '}
            <button type="button" onClick={() => changeLanguage('en')}>
                en
            </button>
            <p>
                <TMessage></TMessage>
            </p>
        </div>
    ));
