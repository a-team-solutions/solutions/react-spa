import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import Home from './Home';

storiesOf('Home', module)
    .addDecorator(withKnobs)
    .add('default', () => (
        <Home
            buttonClickMessage={text('buttonClickMessage', 'textik')}
            users={[]}
            onButtonClick={action('onButtonClick')}
            tableClickMessage={text('tableClickMessage', 'textik')}
            onTableRowClick={action('onTableRowClick')}
        />
    ));
