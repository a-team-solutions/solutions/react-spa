import React from "react";
import { withTranslation, WithTranslation } from "react-i18next";
import i18next from "i18next";
import moment from "moment";
import numeral from "numeral";
import Table from "./Table";
import { User } from "../../../api/generated";

interface Props extends WithTranslation {
    buttonClickMessage: string;
    users: User[];
    tableClickMessage: string;
    onButtonClick?: (data: string) => void;
    onTableRowClick?: (id: number) => void;
}

function Home(props: Props) {
    const { t, buttonClickMessage, users, onButtonClick, tableClickMessage, onTableRowClick } = props;

    function _onButtonClick(data: string) {
        onButtonClick?.(data);
    }

    const usersHead: (keyof User)[] = ["name", "login", "password", "roles"];
    const usersData = users.map(u => usersHead.map(k => u[k].toString()));

    return (
        <section className="section">
            <div className="container">
                <h1 className="title is-1">{t('Home.title')}</h1>
                <h2 className="title is-3">i18n</h2>
                <h2 className="subtitle is-4">Lang: {i18next.language}</h2>
                <p>
                    {moment(new Date()).format('LLL')}
                    <br/>
                    {numeral(123456.789).format('0,0.00')}
                </p>
                <hr/>
                <h2 className="title is-3">action</h2>
                <p className="control">
                    <button className="button is-primary"
                        onClick={() => _onButtonClick('home-button')}>
                            {t('common.clickme')}
                    </button>
                </p>
                <p>
                    {buttonClickMessage}
                </p>
                <hr/>
                <h2 className="title is-3">Users</h2>
                <Table
                    head={usersHead}
                    data={usersData}
                    onRowClick={onTableRowClick}
                />
                <p>
                    {tableClickMessage}
                </p>
            </div>
        </section>
    );
}

export default withTranslation()(Home);
