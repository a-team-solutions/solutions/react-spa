import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import React from 'react';
import Table from './Table';
import { withKnobs, array, object } from '@storybook/addon-knobs';

const colls = 3;
const rows = 10;

const head = Array.from(Array(colls), (value, index) => `Head [${index + 1}]`);
const data = Array.from(Array(rows), (val, index) => index + 1).map(row =>
    Array.from(Array(colls), (val, index) => index + 1).map(
        cell => `Cell ${row},${cell}`
    )
);

storiesOf('Table', module)
    .addDecorator(withKnobs)
    .add('default', () => (
        <div className="t-panel">
            <Table
                head={array('Head', head)}
                data={object('Data',{ data }).data}
                onRowClick={action('row clicked')}
            ></Table>
        </div>
    ));
