import React from 'react';

interface Props {
    head: string[];
    data: string[][];
    onRowClick?: (id: number) => void;
}

function Table(props: Props) {
    const { head, data, onRowClick } = props;

    return (
        <table className="table is-striped is-bordered">
            <thead>
                <tr>
                    {head.map((head, id) => (
                        <th key={id}>{head}</th>
                    ))}
                </tr>
            </thead>
            <tbody>
                {data.map((row, id) => (
                    <tr key={id} onClick={e => onRowClick?.(id)}>
                        {row.map((cell, id) => (
                            <td key={id}>{cell}</td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default Table;
