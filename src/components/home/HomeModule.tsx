import React, { Component } from 'react';
import { Logic } from '../../logic/logic';
import { WithTranslation, withTranslation } from 'react-i18next';
import Home from './ui/Home';
import { User } from '../../api/generated';

interface Props extends WithTranslation {
    logic: Logic;
}

interface State {
    buttonClickMessage: string;
    users: User[];
    tableClickMessage: string;
}

class HomeModule extends Component<Props, State> {

    private _logic: Logic;

    constructor(props: Props) {
        super(props);
        this._logic = props.logic;
        this._logic.api().user.find().then(users => {
            this.setState({
                users
            });
        });
        this.state = {
            buttonClickMessage: '',
            users: [],
            tableClickMessage: ''
        };
    }

    componentDidMount() {
        (window as any).app.modul = this;
        // Init module
    }

    componentWillUnmount() {
        // Cleanup module
    }

    render() {
        return (
            <Home
                buttonClickMessage={this.state.buttonClickMessage}
                users={this.state.users}
                tableClickMessage={this.state.tableClickMessage}
                onButtonClick={this._onButtonClick}
                onTableRowClick={this._onTableRowClick}
            />
        );
    }

    private _onButtonClick = (data: string) => {
        this.setState({
            buttonClickMessage: `button clicked, data='${data}' (disapper in 3 secunds)`
        });
        setTimeout(() => this.setState({ buttonClickMessage: "" }), 3e3);
    };

    private _onTableRowClick = (id: number) => {
        this.setState({
            tableClickMessage: `table clicked, row='${id}' (disapper in 3 secunds)`
        });
        setTimeout(() => this.setState({ tableClickMessage: "" }), 3e3);
    };

}

export default withTranslation()(HomeModule);
