import React, { Suspense } from "react";
import { HashRouter, Switch, Route } from "react-router-dom";
import { Logic } from "../logic/logic";
import Footer from "./Footer";
import Navbar from "./Navbar";
import HomeModule from "./home/HomeModule";
import FormModule from "./form/FormModule";
import i18next from "i18next";

interface Props {
    logic: Logic;
}

interface State {
    user: string;
}

export enum Routes {
    Home = '/',
    Form = '/form'
}

class App extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            user: 'peter'
        };
        this.props.logic.ga().pageview(window.location.hash);
        window.addEventListener('popstate', () => {
            this.props.logic.ga().pageview(window.location.hash);
        });
    }

    componentDidMount() {
        this.props.logic
            .api().user.find()
            .then(users => {
                this.setState({
                    user: users[0].name || ""
                });
            });
    }

    render() {
        const { logic } = this.props;
        return (
            <Suspense fallback="Loading...">
                <Navbar
                    langs={logic.conf.langs}
                    onLangChange={this._onLangChange}/>
                <HashRouter>
                    <Switch>
                        <Route
                            exact
                            path={Routes.Home}
                            render={() => <HomeModule logic={logic} />}
                        />
                        <Route
                            exact
                            path={Routes.Form}
                            render={() => <FormModule logic={logic} />}
                        />
                    </Switch>
                </HashRouter>
                <Footer text="Hello!"/>
            </Suspense>
        );
    }

    private _onLangChange = (lang: string) => {
        console.log("lang change", lang);
        i18next.changeLanguage(lang);
        this.props.logic.settings().setProp('lang', lang);
    };
}

export default App;
