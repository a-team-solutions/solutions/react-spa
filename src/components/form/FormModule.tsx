import React, { Component } from "react";
import { Logic } from "../../logic/logic";
import { WithTranslation, withTranslation } from "react-i18next";
import Form, { Model } from "./ui/Form";

interface Props extends WithTranslation {
    logic: Logic;
}

interface State {
    form: {
        model: Model;
    };
    data?: Model;
}

class FormModule extends Component<Props, State> {
    private _logic: Logic;

    constructor(props: Props) {
        super(props);
        this._logic = props.logic;
        this.state = {
            form: {
                model: {
                    name: "Jozko",
                    age: 200,
                    married: false,
                    sport: "",
                    sex: ""
                }
            }
        };
    }

    componentDidMount() {
        (window as any).app.modul = this;
        // Init module
    }

    componentWillUnmount() {
        // Cleanup module
    }

    render() {
        return (
            <>
                <Form
                    model={this.state.form.model}
                    onSubmit={this._onFormSubmit}
                    onCancel={this._onFormCancel}
                />
                <pre>{JSON.stringify(this.state.data, null, 4)}</pre>
            </>
        );
    }

    private _onFormSubmit = (value: Model) => {
        console.log("form submit", value);
        this.setState({
            data: value
        });
    };

    private _onFormCancel = () => {
        this.setState({
            data: undefined
        });
    };
}

export default withTranslation()(FormModule);
