import React from "react";
import {
    StringValidator,
    NumberValidator,
    FormValidator,
    SelectValidator,
    BooleanValidator
} from "../../../validators";
import { withTranslation, WithTranslation } from "react-i18next";
import { TFunction } from "i18next";

type SportType = "gymnastics" | "football" | "";
type SexType = "Male" | "Female" | "";

export interface Model {
    name: string;
    age: number;
    married: boolean;
    sex: SexType;
    sport: SportType;
}

interface Props extends WithTranslation {
    model: Model;
    onSubmit: (model: Model) => void;
    onCancel: () => void;
}

interface State {
    name: string;
    nameErr: string;
    age: string;
    ageErr: string;
    married: string;
    marriedErr: string;
    sex: string;
    sexErr: string;
    sport: string;
    sportErr: string;
}

function validator(t: TFunction) {
    const nameValidator = new StringValidator(
        {
            required: true,
            min: 3,
            max: 30,
            regexp: /^(\w{3,15} +\w{3,15})/
        },
        {
            required: "validator.required",
            invalid_format: "validator.invalid_format",
            not_in_range: t("validator.not_in_range")
        }
    );

    const ageValidator = new NumberValidator(
        {
            required: true,
            min: 3,
            max: 100
        },
        {
            required: "validator.required",
            not_in_range: t("validator.not_in_range")
        }
    );

    const sportValidator = new SelectValidator(
        {
            required: true,
            options: ["gymnastics", "football"]
        },
        {
            required: "validator.required"
        }
    );

    const sexValidator = new SelectValidator(
        {
            required: true,
            options: ["Male", "Female"]
        },
        {
            required: "validator.required"
        }
    );

    const marriedValidator = new BooleanValidator(
        {
            required: true,
            // value: false
        },
        {
            required: "validator.required",
            invalid_value: "validator.invalid_value"
        }
    );

    const formValidator = new FormValidator<UserForm>()
        .addValidator("name", nameValidator)
        .addValidator("age", ageValidator)
        .addValidator("sport", sportValidator)
        .addValidator("sex", sexValidator)
        .addValidator("married", marriedValidator);

    return formValidator;
}

interface UserForm {
    name: string;
    age: string;
    sport: string;
    sex: string;
    married: string;
}

class Form extends React.Component<Props, State> {
    model: Model;

    constructor(props: Props) {
        super(props);
        const formatRes = validator(props.t).format(props.model);

        this.model = { ...props.model };
        this.state = {
            ...formatRes.str!,
            nameErr: "",
            ageErr: "",
            sexErr: "",
            sportErr: "",
            marriedErr: ""
        };
    }

    render() {
        return (
            <section className="section">
                <div className="container">
                    <h1 className="title is-1">{this.props.t('Form.title')}</h1>
                    <form>
                        <div className="field">
                            <label className="label">
                                {this.props.t("Form.name")}
                            </label>
                            <div className="control">
                                <input
                                    className="input is-danger----"
                                    type="text"
                                    name="name"
                                    value={this.state.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                            <p className="help is-danger">
                                {this.props.t(this.state.nameErr)}
                            </p>
                        </div>
                        <div className="field">
                            <label className="label">
                                {this.props.t("Form.age")}
                            </label>
                            <div className="control">
                                <input
                                    className="input is-danger----"
                                    type="text"
                                    name="age"
                                    value={this.state.age}
                                    onChange={this.onChangeAge}
                                />
                            </div>
                            <p className="help is-danger">
                                {this.props.t(this.state.ageErr)}
                            </p>
                        </div>
                        <div className="field">
                            <div className="control">
                                <label className="checkbox">
                                    <input
                                        type="checkbox"
                                        name="married"
                                        checked={this.state.married === "true"}
                                        onChange={this.onChangeMarried}
                                    />{" "}
                                    {this.props.t("Form.married")}
                                </label>
                            </div>
                            <p className="help is-danger">
                                {this.props.t(this.state.marriedErr)}
                            </p>
                        </div>
                        <div className="field">
                            <div className="control">
                                <label className="radio">
                                    <input
                                        type="radio"
                                        name="gender"
                                        value="Male"
                                        checked={this.state.sex === "Male"}
                                        onChange={this.onChangeSex}
                                    />{" "}
                                    {this.props.t("Form.male")}
                                </label>
                                <br />
                                <label className="radio">
                                    <input
                                        type="radio"
                                        name="gender"
                                        value="Female"
                                        checked={this.state.sex === "Female"}
                                        onChange={this.onChangeSex}
                                    />{" "}
                                    {this.props.t("Form.female")}
                                </label>
                            </div>
                            <p className="help is-danger">
                                {this.props.t(this.state.sexErr)}
                            </p>
                        </div>
                        <div className="field">
                            <div className="control">
                                <div className="select">
                                    <select
                                        name="sport"
                                        onChange={this.onChangeSport}
                                        value={this.state.sport}
                                    >
                                        <option value="" disabled>
                                            {this.props.t("Form.selectSport")}
                                        </option>
                                        <option value="football">
                                            {this.props.t("Form.football")}
                                        </option>
                                        <option value="gymnastics">
                                            {this.props.t("Form.gymnastics")}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <p className="help is-danger">
                                {this.props.t(this.state.sportErr)}
                            </p>
                        </div>
                        <div className="field is-grouped">
                            <div className="control">
                                <button
                                    className="button is-primary"
                                    type="submit"
                                    onClick={this.onSubmit}
                                >
                                    {this.props.t("common.submit")}
                                </button>
                            </div>
                            <div className="control">
                                <button
                                    className="button is-light"
                                    type="button"
                                    onClick={this.onCancel}
                                >
                                    {this.props.t("common.cancel")}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        );
    }

    private onChangeName = (event: React.ChangeEvent) => {
        const value = (event.target as HTMLInputElement).value;
        const validateRes = validator(this.props.t).validators.name.validate(value);
        if (!validateRes.err) {
            this.model.name = validateRes.obj as string;
        }
        this.setState({
            name: value,
            nameErr: validateRes.err as string
        });
    };

    private onChangeAge = (event: React.ChangeEvent) => {
        const value = (event.target as HTMLInputElement).value;
        const validateRes = validator(this.props.t).validators.age.validate(value);
        if (!validateRes.err) {
            this.model.age = validateRes.obj as number;
        }
        this.setState({
            age: value,
            ageErr: validateRes.err as string
        });
    };

    private onChangeMarried = (event: React.ChangeEvent) => {
        const value = (event.target as HTMLInputElement).checked;
        const validateRes = validator(this.props.t).validators.married.validate(value);
        if (!validateRes.err) {
            this.model.married = validateRes.obj as boolean;
        }
        this.setState({
            married: "" + value,
            marriedErr: validateRes.err as string
        });
    };

    private onChangeSex = (event: React.ChangeEvent) => {
        const value = (event.target as HTMLInputElement).value;
        const validateRes = validator(this.props.t).validators.sex.validate(value);
        if (!validateRes.err) {
            this.model.sex = validateRes.obj as SexType;
        }
        this.setState({
            sex: value,
            sexErr: validateRes.err as string
        });
    };

    private onChangeSport = (event: React.ChangeEvent) => {
        const value = (event.target as HTMLInputElement).value;
        const validateRes = validator(this.props.t).validators.sport.validate(value);
        if (!validateRes.err) {
            this.model.sport = validateRes.obj as SportType;
        }
        this.setState({
            sport: value,
            sportErr: validateRes.err as string
        });
    };

    private onSubmit = (event: React.FormEvent) => {
        event.preventDefault();
        const validateRes = validator(this.props.t).validate({
            name: this.state.name,
            age: this.state.age,
            sport: this.state.sport,
            sex: this.state.sex,
            married: this.state.married
        });
        console.log('form validation', validateRes);
        if (validateRes.valid) {
            this.props.onSubmit(this.model);
        } else {
            this.setState({
                nameErr: validateRes.err!.name,
                ageErr: validateRes.err!.age,
                sexErr: validateRes.err!.sex,
                sportErr: validateRes.err!.sport,
                marriedErr: validateRes.err!.married
            });
        }
    };

    private onCancel = () => {
        this.props.onCancel();
    };

}

export default withTranslation()(Form);
