import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import React, { Fragment } from 'react';
import i18next from 'i18next';
import Form, { Model } from './Form';
import { withKnobs, object } from '@storybook/addon-knobs';

function changeLanguage(lang: string): void {
    console.log(lang);
    i18next.changeLanguage(lang);
}

const model: Model = {
    name: 'Jozko',
    age: 200,
    married: false,
    sport: '',
    sex: ''
};

storiesOf('Form', module)
    .addDecorator(withKnobs)
    // .addDecorator(i18nDecorator)
    .add('default', () => (
        <Fragment>
            <button type="button" onClick={() => changeLanguage('sk')}>
                sk
            </button>{' '}
            <button type="button" onClick={() => changeLanguage('en')}>
                en
            </button>
            <Form
                model={object('Model', model)}
                onSubmit={action('onSubmit')}
                onCancel={action('onCancel')}
            />
        </Fragment>
    ));
