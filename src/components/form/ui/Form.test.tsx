import React from 'react';
import { render } from '@testing-library/react';
import Form from './Form';

test('renders learn react link', () => {
  const { getByText } = render(<Form
    model={{
        name: 'Jozko',
        age: 200,
        married: false,
        sport: '',
        sex: ''
    }}
    onSubmit={model =>
        console.log(
            'A name was submitted: ' +
                JSON.stringify(model, null, 4)
        )
    }
    onCancel={() => console.log('onClickCancel')}
/>);
  const linkElement = getByText(/Docs/i);
  expect(linkElement).toBeInTheDocument();
});
