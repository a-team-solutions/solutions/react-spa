import React from 'react';
import { WithTranslation, withTranslation } from 'react-i18next';

interface Props extends WithTranslation {
    text: string;
}

function Footer(props: Props) {
    return (
        <footer className="footer">
            <div className="content has-text-centered">
                <p>
                    {props.t('Footer.text')}:
                    {' '}
                    {props.text},
                </p>
            </div>
        </footer>
    );
}

export default withTranslation()(Footer);
