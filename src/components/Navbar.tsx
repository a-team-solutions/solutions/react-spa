import React from 'react';
import { Routes } from './App';
import { WithTranslation, withTranslation } from 'react-i18next';
import Clock from './_common/Clock';

interface Props extends WithTranslation {
    langs: string[];
    onLangChange?: (lang: string) => void;
}

function Navbar(props: Props) {
    // function getLanguage() {
    //     return i18next.language || window.localStorage.i18nextLng;
    // }

    function onLangChange(lang: string) {
        return (e: React.MouseEvent<HTMLAnchorElement, globalThis.MouseEvent>): void => {
            e.preventDefault();
            props.onLangChange?.(lang);
        }
    }

    const { t, langs } = props;

    return (
        <nav className="navbar" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <a className="navbar-item" href="#/">
                    {/* <img alt="" src="https://bulma.io/images/bulma-logo.png" width="112" height="28"/> */}
                    <b>React SPA</b>
                </a>
                <a href="#/" role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>
            <div id="navbarBasicExample" className="navbar-menu">
                <div className="navbar-start">
                    <a className="navbar-item" href={`#${Routes.Home}`}>
                        {t('Navbar.home')}
                    </a>
                    <a className="navbar-item" href={`#${Routes.Form}`}>
                        {t('Navbar.form')}
                    </a>
                    <a className="navbar-item" href="storybook">
                        Storybook
                    </a>
                </div>
                <div className="navbar-end">
                    <div className="navbar-item has-dropdown is-hoverable">
                        <a href="#/" className="navbar-link">
                            {t('Navbar.language')}
                        </a>
                        <div className="navbar-dropdown">
                            {langs.map(lang =>
                                <a href="#/" className="navbar-item"
                                    key={lang}
                                    onClick={onLangChange(lang)}>
                                    {t(`Navbar.lang.${lang}`)}
                                </a>)
                            }
                            <hr className="navbar-divider"/>
                            <a href="#/" className="navbar-item"
                                onClick={onLangChange('en')}>
                                {t('Navbar.lang.default')}
                            </a>
                        </div>
                    </div>
                    <div className="navbar-item">
                        <Clock />
                    </div>
                </div>
            </div>
        </nav>
    );
}

export default withTranslation()(Navbar);
