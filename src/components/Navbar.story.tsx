import { storiesOf } from '@storybook/react';
import { withKnobs, array } from '@storybook/addon-knobs';
import React from 'react';
import { action } from '@storybook/addon-actions';
import Navbar from './Navbar';
import { confDefault } from '../conf';

storiesOf('Navbar', module)
    .addDecorator(withKnobs)
    .add('default', () => (
        <Navbar
            langs={array('Langs', confDefault.langs)}
            onLangChange={action('onLangChange')}
        />
    ));
