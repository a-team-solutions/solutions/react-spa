import { Logic } from './logic';

import { confDefault } from '../conf';

test('Logic', async () => {
    const logic = new Logic(confDefault);

    expect(logic).toBeDefined();
});
