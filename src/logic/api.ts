import { UserApi, Configuration, RequestContext, FetchParams, ResponseContext } from "../api/generated";

type Auth = () => Promise<string | undefined>;

export class Api {

    readonly basePath: string;
    readonly user: UserApi;

    private _auth?: Auth;

    constructor(basePath: string) {
        this.basePath = basePath;

        const apiConf = new Configuration({
            basePath: this.basePath,
            middleware: [
                {
                    pre: async (
                        context: RequestContext
                    ): Promise<FetchParams | void> => {
                        const token = this._auth && await this._auth();
                        context.init.headers = {
                            ...context.init.headers,
                            Authorization: 'Bearer ' + token
                        };
                        (context.init as any).t0 = performance.now();
                        return Promise.resolve(context);
                    },
                    post: async (
                        context: ResponseContext
                    ): Promise<Response | void> => {
                        const t1 = performance.now();
                        const t = t1 - (context.init as any).t0;
                        const limit = 100e1;
                        if (t > limit) {
                            console.warn(
                                `API ${context.url ||
                                    ''} ${t} ms (request exceeded ${limit} ms)\n`,
                                context.init.body,
                                await context.response.json()
                            );
                        }
                        return;
                    }
                }
            ]
        });

        this.user = new UserApi(apiConf);
    }

    init(auth?: Auth) {
        auth && (this._auth = auth);
    }

}
