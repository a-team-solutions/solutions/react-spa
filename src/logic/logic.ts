import { Conf } from '../conf';
import { Settings } from './settings';
import { Api } from './api';
import { GoogleAnalyticsLogic } from './google-analytics';

export class Logic {
    readonly conf: Conf;

    private _settings?: Settings<Conf['settings']>;
    private _api?: Api;
    private _ga?: GoogleAnalyticsLogic;

    constructor(conf: Conf) {
        this.conf = conf;
    }

    settings() {
        if (!this._settings) {
            const settings = JSON.parse(localStorage.getItem('settings') || '{}');
            this._settings = new Settings<Conf['settings']>(
                    {
                        ...this.conf.settings,
                        ...settings
                    }
                )
                .onChange(() => localStorage.setItem(
                    'settings', JSON.stringify(this._settings?.getProps())));
        }
        return this._settings;
    }

    api() {
        if (!this._api) {
            this._api = new Api(this.conf.api.basePath);
            // this._api.init(auth);
        }
        return this._api;
    }

    ga() {
        if (!this._ga) {
            this._ga = new GoogleAnalyticsLogic(this.conf.googleAnalytics);
        }
        return this._ga;
    }
}
