import React from 'react';
import ReactDOM from 'react-dom';
import { Conf, confDefault } from './conf';
import i18nInit from './i18n';
import { Logic } from './logic/logic';
import App from './components/App';
// import { scripts } from 'load';
import * as serviceWorker from './serviceWorker';

window.onerror = (message, source, lineno, colno, error) => {
    if (error?.stack) {
        console.error(message, source, lineno, colno, error);
        // TODO: send error to rever for processing
        // http.post(conf.onerror).send({
        //     stack: error.stack,
        //     bundle,
        //     source
        // });
    }
};

const conf: Conf = { ...confDefault, ...(window as any).CONF };

console.log('VERSION', conf.version);
console.log('conf', conf);

const logic = new Logic(conf);

console.log('settings', logic.settings().getProps());

// logic.ga().initialize(userId);

// const authToken = async (): Promise<string | undefined> => {
//     await auth.updateToken();
//     return auth.token();
// };

// logic
//     .notification()
//     .connect(authToken)
//     .catch(console.error);

// logic.api().init(authToken);

// logic.notification().any(message => {
//     console.log(
//         `Notification: ${message.type} ${JSON.stringify(
//             message.data,
//             null,
//             4
//         )}`
//     );
// });

const i18n = i18nInit(
    logic.settings().getProp('lang') || conf.lang
);

(window as any).app = {
    version: conf.version,
    conf,
    i18n,
    logic,
    // test: () =>
    //     scripts([
    //         'https://cdn.jsdelivr.net/npm/tape-standalone@4.4.0/tape.js',
    //         'test.js'
    //     ])
};

// // Run remote test
// // console: app.logic.notification().send({ type: 'test' }, app.logic.auth().user().id);
// logic.notification().on('test', msg => {
//     console.log(msg.type);
//     (window as any).app.test();
//     // Send result to server
// });

ReactDOM.render(<App logic={logic} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
