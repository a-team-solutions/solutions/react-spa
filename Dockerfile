FROM node:12.14.1-alpine as builder

RUN apk add git

WORKDIR /app
COPY package* /app/
RUN npm install

ADD . /app

ARG REACT_APP_VERSION
ENV REACT_APP_VERSION=$REACT_APP_VERSION

ENV NPM_CONFIG_LOGLEVEL info

RUN npm run build \
    && npm run build-storybook \
    && mv -v storybook-static build/storybook \
    && ls -l build \
    && cat build/conf.js


FROM nginx:1.9.15-alpine

COPY --from=builder /app/package.json  /usr/share/nginx/html/package.json
COPY --from=builder /app/build/        /usr/share/nginx/html
# COPY --from=builder /nginx.conf        /etc/nginx/conf.d/default.conf

RUN ls -la /usr/share/nginx/html \
    && ls -l /etc/nginx/conf.d \
    && cat /etc/nginx/nginx.conf \
    && cat /etc/nginx/conf.d/default.conf
