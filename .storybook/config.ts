import { configure } from '@storybook/react';

import i18nInit from '../src/i18n';

require('../public/conf');

// hot-reload css
// import '../public/css/default.css';

import './storybook.css'

// automatically import all files ending in *.stories.tsx https://storybook.js.org/docs/guides/guide-react/
const req = require.context('../src/', true, /\.story.tsx?$/);

function loadStories() {
    req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);

i18nInit('en');
